import logo from './logo.svg'
import FormInput from './components/FormInput'
import TodoItem from './components/TodoItem'
import Button from './components/Button'
import React from 'react'
import './App.css';

class App extends React.Component {
  state = {
    todos: [
      {
        id: 1,
        title: "reading a book"
      },
      {
        id: 2,
        title: "workout training"
      },
      {
        id: 3,
        title: "workout training"
      }
    ]
  }
  deleteTask = id => {
    this.setState({
      todos: this.state.todos.filter(item=>item.id != id)
    })
  }

  addTask = data => {
    const id = this.state.todos.length
    const newData = {
      id: id + 1,
      title: data
    }
    this.setState({
      todos: [...this.state.todos, newData]
    })
  }
  render () {
    const { todos } = this.state;
    return (
      <div className="App">
      <div className='logo'>
        <img src={logo} alt='logo' ></img>
        <h3>Task List</h3>
      </div>
      <div className='formInput'>
        <FormInput add={this.addTask}></FormInput>
      </div>
      <div className='todo'>
        {todos.map(item => 
            <TodoItem key={item.id} todo={item} del={this.deleteTask}/>
          )};
      </div>
    </div>
    );
  }
} 

/*
function App () {
  
  return (
    <div className="App">
      <div className='logo'>
        <img src={logo} alt='logo' ></img>
        <h3>Task List</h3>
      </div>
      <div className='formInput'>
        <FormInput></FormInput>
      </div>
      <div className='todo'>
        {todos.map(item => 
            <TodoItem key={item.id} todo={item}/>
          )};
      </div>
    </div>
  )
}
state = {
  todos: [
    {
      id: 1,
      title: "reading a book"
    },
    {
      id: 2,
      title: "workout training"
    }
  ]
}
const { todos } = this.state; */

export default App;
