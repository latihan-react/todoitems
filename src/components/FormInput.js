import React from "react"
import { Container, Card, Table, Button } from 'react-bootstrap';
import { MDBBtn, MDBInputGroup } from 'mdb-react-ui-kit';

class FormInput extends React.Component{
    state = {
        text: ""
    }
    change = e => {
        this.setState({text: e.target.value})

    }
    submit = e => {
        e.preventDefault()
        if(this.state.text !== ""){ //validasi jika data tidak kosong
            this.props.add(this.state.text) //memanggil props add dari app.js dan state yg di atas dengan variable text
        }
        this.setState({ //fungsi setState ini untuk mengembalikan text pada inputan setelah di input
            text: ""
        })
    }
    render() {
        return (
            <Container>
            <Card>
                <Card.Body >
                    <form onSubmit={this.submit}>
                    <div className="container d-flex justify-content-between align-items-center">

                        <MDBInputGroup>
                            <input className='form-control' placeholder="Input Todo" value ={this.state.text} type='text' onChange={this.change}/>
                            <MDBBtn outline action={this.submit}>Submit</MDBBtn>
                        </MDBInputGroup>
                        </div>
                        </form>
                </Card.Body>
            </Card>
        </Container>
        )
    }
}



export default FormInput