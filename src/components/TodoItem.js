import react from "react"
import { Container, Card, Table } from 'react-bootstrap'
import { MDBBtn } from 'mdb-react-ui-kit'
import Button from "./Button";
import PropTypes from "prop-types";

const TodoItem = ({ todo, del }) => {
    const delById = id => {
        del(id)
    }
    return (
        <Container>
            <Card className="rounded-pill mt-3">
                <Card.Body>
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="col-md-4 d-flex align-items-center">
                        <h1>{todo.title}</h1>
                        </div>
                        <div className="justify-content-end ps-2">
                        <Button text="EDIT"/>
                        <Button text="DELETE" action = {()=> delById(todo.id)}/>
                            
                        </div>
                    </div>
                
                </Card.Body>
            </Card>
        </Container>
    )
}

TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    del: PropTypes.func.isRequired,
    action: PropTypes.func
}

export default TodoItem