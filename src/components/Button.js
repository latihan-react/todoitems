import React from "react"
import { MDBBtn } from 'mdb-react-ui-kit';
import PropTypes from 'prop-types';

const Button = ({ text, action, variant}) => {
    return (
        <MDBBtn outline rounded className="mx-2" onClick={action} variant="">
            {text} 
        </MDBBtn>
        
    )
}

Button.prototype = {
    text: PropTypes.string.isRequired,
    action: PropTypes.func,
    variant: PropTypes.string
}
export default Button